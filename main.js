var danhSachSV=[];
var validatorSV= new ValidatorSV();

// Lấy dữ liệu từ localStorage khi user tải lại trang 
const DSSV_LOCALSTORAGE ="DSSV_LOCALSTORAGE";
var dssvJson=localStorage.getItem("DSSV_LOCALSTORAGE");


const timKiemvitri=function(id,array){
    return array.findIndex(function(sv){
        return sv.maSV== id;
    })
}

// convert array bằng JSON để có thế lưu vào localStorage
const luuLocalStorage=function(){
var dssvJson= JSON.stringify(danhSachSV);

localStorage.setItem(DSSV_LOCALSTORAGE,dssvJson);

}


// Gán cho array gốc gốc và rander lại giao diện

if(dssvJson){
    danhSachSV=JSON.parse(dssvJson);
    danhSachSV=danhSachSV.map(function(item){
        return new SinhVien(
            item.maSV,
            item.tenSV,
            item.emailSV,
            item.toan,
            item.ly,
            item.hoa
        )
    })
    xuatDanhsachsv(danhSachSV);
}



function themSinhVien(){
 var newSinhVien =   layThongTinTuForm();

 var isValid=true;
 var index=danhSachSV.findIndex(function(item){
     return item.maSV == newSinhVien.maSV;
 })

 var isValidMaSV= validatorSV.kiemTrarong("txtMaSV","spanMaSV","Mã sinh viên không đc rỗng")&& validatorSV.kiemTranIdHopLe(newSinhVien,danhSachSV);

 

/* 1 dấu & thì chạy tất cả các thằng sau còn && sai lúc đầu thì sẽ ngừng chạy các thằng sau */


  /* Kiểm tra */
 isValid=validatorSV.kiemTranIdHopLe(newSinhVien,danhSachSV) &validatorSV.kiemTrarong("txtEmail","spanEmailSV","Email không đc rỗng")&
 validatorSV.kiemTrarong("txtMaSV","spanMaSV","Mã không đc rỗng")&
 validatorSV.kiemTrarong("txtTenSV","spanTenSV","Tên không đc rỗng");
  

// taọ biến riêng biệt 
// muốn kiểm tra
var isValidMaSV= validatorSV.kiemTrarong("txtEmail","spanEmailSV","Email không đc rỗng")&& validatorSV.kiemTranIdHopLe(newSinhVien,danhSachSV);
var isValidEmail=  validatorSV.kiemTraEmail("txtEmail","spanEmailSV");
 console.log({isValidEmail});

 console.log(index);
 if(index == -1){
 danhSachSV.push(newSinhVien);
 xuatDanhsachsv(danhSachSV);
 document.getElementById("spanMaSV").innerText="";
 document.getElementById("spanTenSV").innerText="";

  luuLocalStorage();
 }
 else{
     document.getElementById("spanMaSV").innerText=`Mã sinh viên 0 đc trùng`;
     document.getElementById("spanTenSV").innerText=`Tên sinh viên 0 đc trùng`;
 }
 var isValid=isValidMaSV&&isValidEmail;

}


function xoaSV(id){
    console.log(id);
    
    var viTri =timKiemvitri(id,danhSachSV);
    console.log({viTri});
    // xóa tại vị trí tìm thấy với số lượng là 1
    danhSachSV.splice(viTri,1);
    xuatDanhsachsv(danhSachSV);
    luuLocalStorage();
}


function suaSinhvien(id){
    var  viTri=timKiemvitri(id,danhSachSV);
    console.log({viTri});
// 
    var sinhVien= danhSachSV[viTri];
    xuatThongTinTuForm(sinhVien);
}

function capNhatSV (){
    var sinhVienEdit=layThongTinTuForm();
    console.log({sinhVienEdit});
// tìm ra vị trí , tới vị trí đó thay đổi dữ liệu 
    let viTri= timKiemvitri(sinhVienEdit.maSV,danhSachSV);
    danhSachSV[viTri] = sinhVienEdit;
    xuatDanhsachsv(danhSachSV);
    luuLocalStorage();
}

